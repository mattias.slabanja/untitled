import pygame
import numpy as np
import math
from collections import deque

pygame.init()

window_width = 1280
window_height = 1280

x_cells = 32
y_cells = 32

cell_width = window_width / x_cells
cell_height = window_height / y_cells

black = (0, 0, 0)
white = (255, 255, 255)
red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)

window = pygame.display.set_mode((window_width, window_height))
pygame.display.set_caption("Masken")
window.fill(black)


def get_cell_coords(x, y):
    return (math.floor(x * cell_width),
            math.floor(y * cell_height))


def draw_cell(x, y, color):
    x_pixel, y_pixel = get_cell_coords(x, y)
    pygame.draw.rect(window, color, (x_pixel, y_pixel, cell_width, cell_height))




class SnakeState:
    def __init__(self, x_cells, y_cells, start_location):
        self._x_cells = x_cells
        self._y_cells = y_cells
        self._status = "ok"
        self._growing = 0
        self._occupied = np.zeros((x_cells, y_cells), int)
        self._locations = deque([start_location])

    def get_location(self):
        return self._locations[0]

    def get_status(self):
        return self._status

    def get_length(self):
        return len(self._locations)

    def grow(self, amount):
        assert(amount > 0)
        self._growing += amount

    def _next_tentative_location(self, direction):
        x, y = self.get_location()
        if direction == "right":
            return x + 1, y
        if direction == "left":
            return x - 1, y
        if direction == "up":
            return x, y - 1
        if direction == "down":
            return x, y + 1
        return x, y

    def _check_tentative_location(self, tentative_location):
        x, y = tentative_location
        if x < 0 or x >= self._x_cells or y < 0 or y >= self._y_cells:
            return "wall"
        if self._occupied[x, y] != 0:
            return "self"
        return "ok"

    def _push_new_location(self, new_location):
        x, y = new_location
        self._locations.appendleft((x, y))
        self._occupied[x % self._x_cells, y % self._y_cells] = 1

    def _pop_vacated_location(self):
        vacated_location = self._locations.pop()
        x, y = vacated_location
        self._occupied[x, y] = 0
        return vacated_location

    def go_to_new_location(self, direction):
        assert(self._status == "ok")
        tentative_location = self._next_tentative_location(direction)
        self._status = self._check_tentative_location(tentative_location)
        self._push_new_location(tentative_location)

        if self._growing > 0:
            self._growing -= 1
        else:
            return self._pop_vacated_location()

    def get_random_free_location(self):
        free = np.where(self._occupied == 0)
        n_free = len(free[0])
        if n_free > 0:
            ix = np.random.randint(n_free)
            return free[0][ix], free[1][ix]


food_location = None

def do_step(state, direction):
    global food_location

    vacated_location = state.go_to_new_location(direction)
    status = state.get_status()
    if status == "ok":
        draw_cell(*state.get_location(), white)
    else:
        draw_cell(*state.get_location(), red)

    if vacated_location is not None:
        draw_cell(*vacated_location, black)

    if state.get_location() == food_location:
        state.grow(4)
        food_location = None

    if food_location is None:
        food_location = state.get_random_free_location()
        draw_cell(*food_location, green)

    pygame.display.flip()

    return status == "ok"


state = SnakeState(x_cells, y_cells, (x_cells // 2, y_cells // 2))

time_event = pygame.USEREVENT + 1
pygame.time.set_timer(time_event, 100)

direction = None

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                direction = "left"
            elif event.key == pygame.K_RIGHT:
                direction = "right"
            elif event.key == pygame.K_UP:
                direction = "up"
            elif event.key == pygame.K_DOWN:
                direction = "down"
        elif event.type == time_event:
            if direction is not None:
                running = do_step(state, direction)


font = pygame.font.Font('freesansbold.ttf', 32)
text = font.render(f'Game over, you reached length {state.get_length()}', True, red, blue)
text_rectangle = text.get_rect()
text_rectangle.center = (window_width // 2, window_height // 2)
pygame.display.get_surface().blit(text, text_rectangle)
pygame.display.flip()

import time
time.sleep(4)

pygame.quit()